"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Lot from '../../../openxum-core/games/lot/index.mjs';

class Manager extends OpenXum.Manager {
  constructor(t, e, g, o, s, f) {
    super(t, e, g, o, s, f);
    this.that(this);
  }

  build_move() {
    return new Lot.Move();
  }

  get_current_color() {
    return this._engine.current_color() === Lot.Color.WHITE ? 'White' : 'Black';
  }

  static get_name() {
    return 'lot';
  }

  get_winner_color() {
    let str ;
    if(this.engine().winner_is() === Lot.Color.WHITE)
      str = "White";
    else if(this.engine().winner_is() === Lot.Color.BLACK)
      str = "Black";
    else if(this.engine().winner_is() === Lot.Color.GREY)
      str = "Draw";
    return str;
  }

  process_move() {
    //TODO
  }

}

export default Manager;
