"use strict";

import Graphics from '../../graphics/index.mjs';
import Lot from '../../../openxum-core/games/lot/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

class Gui extends OpenXum.Gui {
  constructor(c, e, l, g) {
    super(c, e, l, g);
    this._deltaX = 0;
    this._deltaY = 0;
    this._offsetX = 0;
    this._offsetY = 0;
    this._posX = -1;
    this._posY = -1;
    this._selected_color = this._engine.current_color();
  }

  draw() {
    this._context.lineWidth = 1;
    this._context.strokeStyle = "#592d12";
    this._context.fillStyle = "#592d12";

    Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

    this._draw_grid();
    this._draw_state();
  }

  get_move() {
    if(this._engine._phase === Lot.Phase.PUT_PIECE)
      return new Lot.Move(Lot.MoveType.PUT_PIECE, this._engine._current_color,this._posX,this._posY);
    else if (this._engine._phase === Lot.Phase.SET_STACK)
      return new Lot.Move(Lot.MoveType.SET_STACK, this._engine._current_color_temp,this._posX,this._posY);
    else if (this._engine._phase === Lot.Phase.SELECT_LINE)
      return new Lot.Move(Lot.MoveType.SELECT_LINE, this._engine._current_color,this._posX,this._posY);
  }

  is_animate() {
    return false;
  }

  is_remote() {
    return false;
  }

  move(move, color) {
    this._manager.play();
    // TODO !!!!!
  }

  unselect() {
    this._selected_color = Lot.Color.NONE;
    this.draw();
  }

  set_canvas(c) {
    super.set_canvas(c);
    this._canvas.addEventListener("click", (event) => {
      this._get_click_position(event);
      if (this._posX >= 0 && this._posX < 7 && this._posY >= 0 && this._posY < 7) this._on_click();
    });
    this._deltaX = Math.floor((this._canvas.width * 0.95 - 40) / 7);
    this._deltaY = Math.floor((this._canvas.height * 0.95 - 40) / 7);
    this._offsetX = Math.floor(this._canvas.width / 2 - this._deltaX * 3.5);
    this._offsetY = Math.floor(this._canvas.height / 2 - this._deltaY * 3.5);
    this.draw();
  }

  _draw_grid() {
    this._context.lineWidth = 10;
    let i, j, color_case_board = 1;
    for (i = 0; i < 7; ++i) {
      for (j = 0; j < 7; ++j) {
        this._context.beginPath();
        if (color_case_board === 1) {
          this._context.fillStyle = '#a7732b';
          color_case_board = 2;
        } else {
          color_case_board = 1;
          this._context.fillStyle = '#eacb6a';
        }
        this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
        this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + j * this._deltaY);
        this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + (j + 1) * this._deltaY - 2);
        this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY - 2);
        this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
        this._context.closePath();
        this._context.fill();
      }
    }
  }

  _draw_state(){
    this._draw_text();
    for (let y = 0; y < 7; y++) {
      for (let x = 0; x < 7; x++) {
        if (this._engine._board[x][y] !== Lot.Color.NONE)
          this._draw_piece(this._deltaX  * (x + 1), this._deltaY * (y +1), this._engine._board[x][y]);
      }
    }
  }

  _draw_text() {
    this._context.fillStyle = "#ffffff";
    this._context.textAlign = "start";
    this._context.textBaseline = "middle";
    this._context.font = 0.7 * this._offsetY + "px calibri";
    this._context.fillText( "White: " + this._engine._white_pieces_number + "   Black: " + this._engine._black_pieces_number ,this._offsetX, this._offsetY / 2);
  }

  _draw_color_piece(piece, radius) {
    if(Lot.Color.BLACK === piece)
      this._context.fillStyle = "#000";
    else if(Lot.Color.WHITE === piece)
      this._context.fillStyle = "#FFF";
    else if(Lot.Color.BLACK_TEMP === piece) {
      this._context.fillStyle = "#000";
      this._context.strokeStyle = "#ff0000";
    } else if(Lot.Color.WHITE_TEMP === piece){
      this._context.fillStyle = "#FFF";
      this._context.strokeStyle = "#ff0000";
    } else if(Lot.Color.BLACK_STACK === piece){
      radius = this._deltaX / 2.5;
      this._context.fillStyle = "#000";
    } else if(Lot.Color.WHITE_STACK === piece){
      radius = this._deltaX / 2.5;
      this._context.fillStyle = "#FFF";
    }
    return radius;
  }

  _draw_piece(x, y, piece) {
    let radius = this._deltaX / 3.5;
    this._context.lineWidth = 2;
    this._context.strokeStyle = "#000";
    radius = this._draw_color_piece(piece, radius);
    this._context.beginPath();
    this._context.arc(x, y, radius, 0.0, 2 * Math.PI);
    this._context.closePath();
    this._context.fill();
    this._context.stroke();
  }

  _on_click() {
    let ok = false;
    this._selected_color = this._engine.current_color();
    if (this._engine.get_phase() !== Lot.Phase.FINISH) {
      if (this._engine.get_phase() === Lot.Phase.PUT_PIECE && this._engine._board[this.get_move().get_abs()][this.get_move().get_ord()] === Lot.Color.NONE)
        ok = true;
      else if (this._engine.get_phase() === Lot.Phase.SET_STACK && this._engine._board[this.get_move().get_abs()][this.get_move().get_ord()] === this._engine._current_color_temp)
        ok = true;
      else if (this._engine.get_phase() === Lot.Phase.SELECT_LINE && this._engine._board[this.get_move().get_abs()][this.get_move().get_ord()] === this._engine._current_color_temp)
        ok = true;
    } else ok = false;
    if(ok) this._manager.play();
  }

  _get_click_position(event) {
    let rect = this._canvas.getBoundingClientRect();
    let x = (event.clientX - rect.left) * this._scaleX - this._offsetX;
    let y = (event.clientY - rect.top) * this._scaleY - this._offsetY;
    this._posX = Math.floor(x / this._deltaX);
    this._posY = Math.floor(y / this._deltaY);
  }

}

export default Gui;