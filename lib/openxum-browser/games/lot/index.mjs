"use strict";

import Lot from '../../../openxum-core/games/lot/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
  Gui: Gui,
  Manager: Manager,
  Settings: {
    ai: {
      mcts: AI.RandomPlayer
    },
    colors: {
      first: Lot.Color.BLACK,
      init: Lot.Color.BLACK,
      list: [
        {key: Lot.Color.BLACK, value: 'black'},
        {key: Lot.Color.WHITE, value: 'white'}
      ]
    },
    modes: {
      init: Lot.GameType.STANDARD,
      list: [
        {key: Lot.GameType.STANDARD, value: 'standard'}
      ]
    },
    opponent_color(color) {
      return color === Lot.Color.BLACK ? Lot.Color.WHITE : Lot.Color.BLACK;
    },
    types: {
      init: 'ai',
      list: [
        {key: 'gui', value: 'GUI'},
        {key: 'ai', value: 'AI'},
        {key: 'online', value: 'Online'},
        {key: 'offline', value: 'Offline'}
      ]
    }
  }
};
