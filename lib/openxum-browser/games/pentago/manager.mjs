"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Pentago from '../../../openxum-core/games/pentago/index.mjs';

class Manager extends OpenXum.Manager {
  constructor(t, e, g, o, s, w, f) {
    super(t, e, g, o, s, w, f);
    this.that(this);
  }

  build_move() {
    return new Pentago.Move();
  }

  get_current_color() {
    return this.engine().current_color() === Pentago.Color.BLACK ? 'Black' : 'White';
  }

  static get_name() {
    return 'pentago';
  }

  get_winner_color() {
    return this.engine().winner_is() === Pentago.Color.BLACK ? 'black' : 'white';
  }

  process_move() {
  }
}

export default Manager;