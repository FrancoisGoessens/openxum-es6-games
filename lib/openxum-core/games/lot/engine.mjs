//* JEU DE LOT * FURGOL THOMAS * GOESSENS FRANCOIS * TELLIEZ ALEXIS *\\

import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import Phase from './phase.mjs';

class Engine extends OpenXum.Engine {
  constructor() {
    super();
    this._current_color = Color.WHITE;
    this._current_color_temp = Color.WHITE_TEMP;
    this._previous_color = Color.WHITE;
    this._color_win = Color.NONE;
    this._phase = Phase.PUT_PIECE;
    this._black_pieces_number = 45;
    this._white_pieces_number = 45;
    this._nb_temp = 0;
    this._init_board();
    this._coords_line = [];
  }

  build_move() {
    return new Move();
  }

  clone() {
    //TODO
  }

  current_color() {
    return this._current_color;
  }

  get_phase() {
    return this._phase;
  }

  get_name() {
    return 'Lot';
  }

  get_case_board(abs, ord) {
    return this._board[abs][ord];
  }

  get_possible_move_list() {
    let list = [], list_temp_stack = [], list_temp_line = [], phase = this.get_phase();
    if (phase === Phase.SELECT_LINE) {
      list_temp_line = this.get_possible_line();
      for(let i = 0; i < this._nb_temp; i++)
        list.push(list_temp_line[i]);
    } else if (phase === Phase.SET_STACK) {
      list_temp_stack = this.get_possible_stack();
      for(let i = 0; i < this._nb_temp; i++)
        list.push(list_temp_stack[i]);
    } else if (phase === Phase.PUT_PIECE) {
      for (let i = 0; i < 7; i++) {
        for (let j = 0; j < 7; j++) {
          if (this._board[i][j] === Color.NONE)
            list.push(new Move(MoveType.PUT_PIECE, this.current_color(), i, j));
        }
      }
    }
    return list;
  }

  get_possible_stack() {
    let list_temp = [];
    for (let i = 0; i < 7; i++){
      for (let j = 0; j < 7; j++){
        if(this._board[i][j] === this._current_color_temp)
          list_temp.push(new Move(MoveType.SET_STACK, this.current_color(), i, j));
      }
    }
    return list_temp;
  }

  get_possible_line() {
    let list_temp = [];
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        if(this._board[i][j] === this._current_color_temp)
          list_temp.push(new Move(MoveType.SELECT_LINE, this.current_color(), i, j));
      }
    }
    return list_temp;
  }

  get_current_color_temp() {
    return this._current_color_temp;
  }

  set_case_board(color, abs, ord) {
    this._board[abs][ord] = color;
  }

  set_coords_line(move) {
    if (this._coords_line.length < 6) {
      if (move.get_abs() !== this._coords_line[0] || move.get_ord() !== this._coords_line[1]) {
        this._coords_line.push(move.get_abs());
        this._coords_line.push(move.get_ord());
      }
    }
  }

  set_phase(phase) {
    this._phase = phase;
  }

  set_current_color_temp(color) {
    this._current_color_temp = color;
  }

  set_number_piece(color, nb) {
    if(color === Color.WHITE) this._white_pieces_number = nb;
    if(color === Color.BLACK) this._black_pieces_number = nb;
  }

  move(move) {
    let _phase = this.get_phase(), _abs = move.get_abs(), _ord = move.get_ord();

    if (_phase === Phase.SELECT_LINE && this._board[_abs][_ord] === this.get_current_color_temp())
      this._select_line(move);
    if (_phase === Phase.SET_STACK && this._board[_abs][_ord] === this.get_current_color_temp())
      this._set_stack(move);
    if (_phase === Phase.PUT_PIECE)
      this._put_piece(move);

    this.launch_verif_win(move);
  }

  change_temp_to_none() {
    for (let x = 0; x < 7; x++){
      for (let y =0; y < 7; y++) {
        if (this._board[x][y] === this.get_current_color_temp()) {
          this._board[x][y] = Color.NONE;
          if (this.current_color() === Color.BLACK)
            this._black_pieces_number++;
          else if (this.current_color() === Color.WHITE)
            this._white_pieces_number++;
        }
      }
    }
  }

  is_finished() {
    if (this.get_phase() === Phase.FINISH)
      return true;
    return false;
  }

  parse(str) {
    //TODO
  }

  to_string() {
    //TODO
  }

  winner_is() {
    return this._color_win;
  }

  _change_current_player() {
    this._previous_color = this._current_color;
    if (this.current_color() === Color.BLACK)
      this._current_color = Color.WHITE;
    else if (this.current_color() === Color.WHITE)
      this._current_color = Color.BLACK;
  }

  _init_board() {
    this._board = new Array(7);

    for (let x = 0; x < 7; x++)
      this._board[x] = new Array(7);

    for (let x = 0; x < 7; x++) {
      for (let y = 0; y < 7; y++)
        this._board[x][y] = Color.NONE;
    }
  }

  _verif_line() {
    this._line = [];
    let Z1 = this._coords_line[0] + 7 * this._coords_line[1];
    let Z2 = this._coords_line[2] + 7 * this._coords_line[3];
    let Z3 = this._coords_line[4] + 7 * this._coords_line[5];
    this._line = [Z1,Z2,Z3];
    this._line.sort((a,b) => a - b);
    let D1 = this._line[1] - this._line[0];
    let D2 = this._line[2] - this._line[1];
    if (D1 === D2) {
      if (D1 === 1) return "HORI";
      else if (D1 === 6) return "RTOL";
      else if (D1 === 7) return "VERTI";
      else if (D1 === 8) return "LTOR";
      else return "NO LINE";
    } else return "NO CONSECUTIVE";
  }

  _count_temp() {
    this._nb_temp = 0;
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        if (this._board[i][j] === this.get_current_color_temp()) this._nb_temp++;
      }
    }
    if (this._nb_temp > 3) this.set_phase(Phase.SELECT_LINE);
    if (this._nb_temp === 3) {
      this._coords_line = [];
      this.set_phase(Phase.SET_STACK);
    }
    return this._nb_temp;
  }

  launch_verif_to_set_stack(move) {
    let x = move.get_abs(), y = move.get_ord(), test = false, color_test = this._verif_stack_verti(move);
    if (color_test !== Color.NONE ) {
      this._board[x][y] = color_test;
      test = true;
    }
    if((color_test = this._verif_stack_hori(move)) !== Color.NONE){
      this._board[x][y] = color_test;
      test = true;
    }
    if((color_test = this._verif_stack_diag_rtol(move)) !== Color.NONE){
      this._board[x][y] = color_test;
      test = true;
    }
    if((color_test = this._verif_stack_diag_ltor(move))!== Color.NONE) {
      this._board[x][y] = color_test;
      test = true;
    }
    if (this._board[x][y] === Color.BLACK && test === true) this._board[x][y] = Color.BLACK_TEMP;
    if (this._board[x][y] === Color.WHITE && test === true) this._board[x][y] = Color.WHITE_TEMP;
    if (test === true) this.set_current_color_temp(this._board[x][y]);
    this._count_temp();
  }

  _verif_stack_hori(move) {
    let nb_left = 0, nb_right = 0, x = move.get_abs(), y = move.get_ord();
    for (let i = x + 1; i < 7; i++) {
      if (x === 6) break;
      if (this._board[x][y] !== this._board[i][y]) break;
      nb_right++;
    }
    for (let i = x - 1; i >= 0; i--) {
      if (x === 0) break;
      if (this._board[x][y] !== this._board[i][y]) break;
      nb_left++;
    }
    if ((nb_left + nb_right) >= 2 && this._board[x][y] === Color.BLACK) {
      for (let i = x + nb_right; i >= x - nb_left; i--)
        this._board[i][y] = Color.BLACK_TEMP;
      return Color.BLACK;
    } else if ((nb_left + nb_right) >= 2 && this._board[x][y] === Color.WHITE) {
      for (let i = x + nb_right; i >= x - nb_left; i--)
        this._board[i][y] = Color.WHITE_TEMP;
      return Color.WHITE;
    } return Color.NONE;
  }

  _verif_stack_verti(move) {
    let nb_up = 0, nb_down = 0, x = move.get_abs(), y = move.get_ord();
    for (let i = y + 1; i < 7; i++) {
      if (y === 6) break;
      if (this._board[x][y] !== this._board[x][i]) break;
      nb_down++;
    }
    for (let i = y - 1; i >= 0; i--) {
      if (y === 0) break;
      if (this._board[x][y] !== this._board[x][i]) break;
      nb_up++;
    }
    if ((nb_down + nb_up) >= 2 && this._board[x][y] === Color.BLACK) {
      for (let i = y + nb_down; i >= y - nb_up; i--)
        this._board[x][i] = Color.BLACK_TEMP;
      return Color.BLACK;
    } else if ((nb_down + nb_up) >= 2 && this._board[x][y] === Color.WHITE) {
      for (let i = y + nb_down; i >= y - nb_up; i--)
        this._board[x][i] = Color.WHITE_TEMP;
      return Color.WHITE;
    } return Color.NONE;
  }

  _verif_stack_diag_rtol(move) {
    let nb_top_right = 0, nb_bottom_left = 0, x = move.get_abs(), y = move.get_ord(), i, j;
    for (i = x + 1, j = y - 1; i >= 0, j < 7; i++, j--) {
      if (i === 7 || j === -1) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      nb_top_right++;
    }
    for (i = x - 1, j = y + 1; i >= 0, j < 7; i--, j++) {
      if (i === -1 || j === 7) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      nb_bottom_left++;
    }
    if (this._board[x][y] === Color.BLACK && (nb_top_right + nb_bottom_left) >= 2) {
      for (i = x + nb_top_right, j = y - nb_top_right; i <= x - nb_bottom_left, j <= y + nb_bottom_left; i--, j++)
        this._board[i][j] = Color.BLACK_TEMP;
      return Color.BLACK;
    } else if (this._board[x][y] === Color.WHITE && (nb_top_right + nb_bottom_left) >= 2) {
      for (i = x + nb_top_right, j = y - nb_top_right; i <= x - nb_bottom_left, j <= y + nb_bottom_left; i--, j++)
        this._board[i][j] = Color.WHITE_TEMP;
      return Color.WHITE;
    } return Color.NONE;
  }

  _verif_stack_diag_ltor(move) {
    let nb_top_left = 0, nb_bottom_right = 0, x = move.get_abs(), y = move.get_ord(), i, j;
    for (i = x + 1, j = y + 1; i < 7, j < 7; i++, j++) {
      if (i === 7 || j === 7) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      nb_bottom_right++;
    }
    for (i = x - 1, j = y - 1; i >= 0, j >= 0; i--, j--) {
      if (i === -1 || j === -1) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      nb_top_left++;
    }
    if (this._board[x][y] === Color.BLACK && (nb_top_left + nb_bottom_right) >= 2) {
      for (i = x - nb_top_left, j = y - nb_top_left; i <= x + nb_bottom_right, j <= y + nb_bottom_right; i++, j++)
        this._board[i][j] = Color.BLACK_TEMP;
      return Color.BLACK;
    } else if (this._board[x][y] === Color.WHITE && (nb_top_left + nb_bottom_right) >= 2) {
      for (i = x - nb_top_left, j = y - nb_top_left; i <= x + nb_bottom_right, j <= y + nb_bottom_right; i++, j++)
        this._board[i][j] = Color.WHITE_TEMP;
      return Color.WHITE;
    } return Color.NONE;
  }

  launch_verif_win(move) {
    let color_win = this._win_hori(move);
    if (color_win !== Color.NONE) this._color_win = color_win;
    if ((color_win = this._win_verti(move)) !== Color.NONE) this._color_win = color_win;
    if ((color_win = this._win_diag_ltor(move)) !== Color.NONE) this._color_win = color_win;
    if ((color_win = this._win_diag_rtol(move)) !== Color.NONE) this._color_win = color_win;
    if ((color_win = this._win_draw()) !== Color.NONE) this._color_win = color_win;
    if (this._color_win !== Color.NONE) this.set_phase(Phase.FINISH);
  }

  _win_hori(move) {
    let nb_left = 1, nb_right = 0, x = move.get_abs(), y = move.get_ord();
    for (let i = x + 1; i < 7; i++) {
      if (x === 6) break;
      if (this._board[x][y] !== this._board[i][y]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_right++;
    }
    for (let i = x - 1; i >= 0; i--) {
      if (x === 0) break;
      if (this._board[x][y] !== this._board[i][y]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_left++;
    }
    if (nb_right + nb_left >= 3) {
      this.set_phase(Phase.FINISH);
      return this._previous_color;
    }
    return Color.NONE;
  }

  _win_verti(move) {
    let nb_up = 1, nb_down = 0, x = move.get_abs(), y = move.get_ord();
    for (let i = y + 1; i < 7; i++) {
      if (y === 6) break;
      if (this._board[x][y] !== this._board[x][i]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_down++;
    }
    for (let i = y - 1; i >= 0; i--) {
      if (y === 0) break;
      if (this._board[x][y] !== this._board[x][i]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_up++;
    }
    if (nb_up + nb_down >= 3) {
      this.set_phase(Phase.FINISH);
      return this._previous_color;
    }
    return Color.NONE;
  }

  _win_diag_rtol(move) {
    let nb_top_right = 1, nb_bottom_left = 0, x = move.get_abs(), y = move.get_ord(), i, j;
    for (i = x + 1, j = y - 1; i >= 0, j < 7; i++, j--) {
      if (i === 7 || j === -1) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_top_right++;
    }
    for (i = x - 1, j = y + 1; i >= 0, j < 7; i--, j++) {
      if (i === -1 || j === 7) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_bottom_left++;
    }
    if (nb_top_right + nb_bottom_left >= 3) {
      this.set_phase(Phase.FINISH);
      return this._previous_color;
    }
    return Color.NONE;
  }

  _win_diag_ltor(move) {
    let nb_top_left = 1, nb_bottom_right = 0, x = move.get_abs(), y = move.get_ord(), i, j;
    for (i = x + 1, j = y + 1; i < 7, j < 7; i++, j++) {
      if (i === 7 || j === 7) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_bottom_right++;
    }
    for (i = x - 1, j = y - 1; i >= 0, j >= 0; i--, j--) {
      if (i === -1 || j === -1) break;
      if (this._board[x][y] !== this._board[i][j]) break;
      if (this._board[x][y] === Color.WHITE_STACK || this._board[x][y] === Color.BLACK_STACK)
        nb_top_left++;
    }
    if (nb_top_left + nb_bottom_right >= 3) {
      this.set_phase(Phase.FINISH);
      return this._previous_color;
    }
    return Color.NONE;
  }

  _win_draw() {
    let nb = 0;
    for (let x = 0; x < 7; x++) {
      for (let y = 0; y < 7; y++) {
        if (this._board[x][y] === Color.NONE || this._board[x][y] === this._current_color_temp ) break;
        nb++;
      }
    }
    if (this._white_pieces_number === 0 && this._black_pieces_number === 0)
      return Color.GREY;
    if (nb === 49)
      return Color.GREY;
    return Color.NONE;
  }

  _put_piece(move) {
    if (this._board[move.get_abs()][move.get_ord()] === Color.NONE && this._black_pieces_number >= 0 && this._white_pieces_number >= 0) {
      this._board[move.get_abs()][move.get_ord()] = this.current_color();
      if (this.current_color() === Color.BLACK) this._black_pieces_number--;
      if (this.current_color() === Color.WHITE) this._white_pieces_number--;
      this.launch_verif_to_set_stack(move);
      if(this.get_phase() === Phase.PUT_PIECE)
        this._change_current_player();
    }
  }

  _set_stack(move) {
    if (move.get_type() === MoveType.SET_STACK) {
      if (this._board[move.get_abs()][move.get_ord()] === Color.BLACK_TEMP && this._black_pieces_number >= 0) {
        this._board[move.get_abs()][move.get_ord()] = Color.BLACK_STACK;
        this._black_pieces_number--;
        this.change_temp_to_none();
      } else if (this._board[move.get_abs()][move.get_ord()] === Color.WHITE_TEMP && this._white_pieces_number >= 0) {
        this._board[move.get_abs()][move.get_ord()] = Color.WHITE_STACK;
        this._white_pieces_number--;
        this.change_temp_to_none();
      }
      if (this.get_phase() !== Phase.FINISH) {
        this.set_phase(Phase.PUT_PIECE);
        this._change_current_player();
      }
    }
  }

  _select_line(move) {
    if (move.get_type() === MoveType.SELECT_LINE) {
      this.set_coords_line(move);
      if (this._coords_line.length === 6) {
        if (this._verif_line() === "NO LINE" || this._verif_line() === "NO CONSECUTIVE")
          this._coords_line = [];
        else {
          for (let i = 0; i < 7; i++) {
            for (let j = 0; j < 7; j++) {
              if (this._board[i][j] === Color.BLACK_TEMP) this._board[i][j] = Color.BLACK;
              if (this._board[i][j] === Color.WHITE_TEMP) this._board[i][j] = Color.WHITE;
            }
          }
          this.set_case_board(this._current_color_temp, this._coords_line[0], this._coords_line[1]);
          this.set_case_board(this._current_color_temp, this._coords_line[2], this._coords_line[3]);
          this.set_case_board(this._current_color_temp, this._coords_line[4], this._coords_line[5]);
        }
      }
      this._count_temp();
    }
  }
}

export default Engine;