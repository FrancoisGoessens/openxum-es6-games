"use strict";

const Phase = {PUT_PIECE: 0, SET_STACK: 1, SELECT_LINE: 2 , FINISH: 3};

export default Phase;
