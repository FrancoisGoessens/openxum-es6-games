"use strict";

const MoveType = {PUT_PIECE: 0, SET_STACK: 1, SELECT_LINE: 2};

export default MoveType;
