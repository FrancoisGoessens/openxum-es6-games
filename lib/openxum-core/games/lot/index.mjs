"use strict";

import Engine from './engine.mjs';
import GameType from './game_type.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import Phase from './phase.mjs';
import Color from './color.mjs';

export default {
  Engine: Engine,
  GameType: GameType,
  Move: Move,
  MoveType: MoveType,
  Phase: Phase,
  Color: Color
}
