"use strict";

import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';

class Move extends OpenXum.Move {

  constructor(type, color, abs, ord) {
    super();
    this._type = type;
    this._color = color;
    this._abs = abs;
    this._ord = ord;
  }

  get_color() {
    return parseInt(this._color);
  }

  get_abs() {
    return this._abs;
  }

  get_ord() {
    return this._ord;
  }

  get_type() {
    return this._type;
  }

  from_object(data) {
    if (this.get_type() === MoveType.SELECT_LINE) {
      return "LINE " + this._color === Color.WHITE ? 'W ' : 'B ';
    }
  }

  decode(str) {
    const type = str.charAt(0);
    const color = str.charAt(1);
    const abs = str.charAt(2);
    const ord = str.charAt(3);

    if (type === 'P') this._type = MoveType.PUT_PIECE;
    else if (type === 'S') this._type = MoveType.SET_STACK;
    else if (type === 'L') this._type = MoveType.SELECT_LINE;

    if (color === 'W') this._color = Color.WHITE;
    else if (color === 'B') this._color = Color.BLACK;

    this._abs = abs;
    this._ord = ord;
  }

  encode() {
    if (this.get_type() === MoveType.PUT_PIECE)
      return ('P' + (this.get_color() === 2 ? 'W' :'B') + this._abs + this._ord);
    else if (this.get_type() === MoveType.SET_STACK)
      return ('S' + (this.get_color() === 2 ? 'W' : 'B') + this._abs + this._ord);
    else if (this.get_type() === MoveType.SELECT_LINE)
      return ('L' + (this.get_color() === 2 ? 'W' : 'B'));
  }

  to_object() {
    return {
      t: this._type,
      c: parseInt(this._color),
      x: this._abs,
      y: this._ord
    };
  }

  to_string() {
    if (this.get_type() === MoveType.PUT_PIECE)
      return ('Put' + (this.get_color() === 2 ? ' White ' : ' Black ') + 'piece at ' + this._abs + ',' + this._ord);
    else if (this.get_type() === MoveType.SET_STACK)
      return ('Set' + (this.get_color() === 2 ? ' White ' : ' Black ') + 'stack at ' + this._abs + ',' + this._ord);
    else if (this.get_type() === MoveType.SELECT_LINE)
      return ('Select' + (this.get_color() === 2 ? ' White ' : ' Black ') + 'line of three');
  }

}

export default Move;
