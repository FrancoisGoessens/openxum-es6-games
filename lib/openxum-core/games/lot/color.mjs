"use strict";

const Color = {GREY: -2, NONE: -1, BLACK: 0, BLACK_STACK: 1, WHITE: 2, WHITE_STACK: 3, BLACK_TEMP: 4, WHITE_TEMP: 5};

export default Color;
