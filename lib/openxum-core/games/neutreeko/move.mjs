"use strict";

import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
  constructor(c1, c2) {
    super();
    this._from = c1;
    this._to = c2;
  }

// public methods
  decode(str) {
    this._from = new Coordinates(parseInt(str.charAt(0)), parseInt(str.charAt(1)));
    this._to = new Coordinates(parseInt(str.charAt(2)), parseInt(str.charAt(3)));
  }

  encode() {
    return this._from.to_string() + this._to.to_string();
  }

  from() {
    return this._from;
  }

  from_object(data) {
    this._from = new Coordinates(data.from.column, data.from.line);
    this._to = new Coordinates(data.to.column, data.to.line);
  }

  to() {
    return this._to;
  }

  to_object() {
    return {
      from: this._from === null ? {column: -1, line: -1} : {column: this._from._column, line: this._from._line},
      to: this._to === null ? {column: -1, line: -1} : {column: this._to._column, line: this._to._line},
    };
  }

  to_string() {
    return 'move piece ' + this.from().to_string() + ' to ' + this.to().to_string();
  }

  formatted_string() {
    return 'move piece ' + this.from().formatted_string() + ' to ' + this.to().formatted_string();
  }
}

export default Move;