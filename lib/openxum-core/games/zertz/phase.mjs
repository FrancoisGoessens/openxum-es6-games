"use strict";

const Phase = {SELECT_MARBLE_IN_POOL: 0, PUT_MARBLE: 1, REMOVE_RING: 2, CAPTURE: 3};

export default Phase;