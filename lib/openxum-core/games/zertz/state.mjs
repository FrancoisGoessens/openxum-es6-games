"use strict";

const State = {VACANT: 0, BLACK_MARBLE: 1, WHITE_MARBLE: 2, GREY_MARBLE: 3, EMPTY: 4};

export default State;