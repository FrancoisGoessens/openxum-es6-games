require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

describe('engine', () => {
  test('_init_board', () => {
    let game = new OpenXum.Lot.Engine();

    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        expect(game.get_case_board(i,j)).toBe(-1);
      }
    }
  });

  test('put_piece', () => {
    let game = new OpenXum.Lot.Engine();

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 0, 5));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 2, 4));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 3, 1));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 6, 0));

    expect(game.get_case_board(0,5)).toBe(2);
    expect(game.get_case_board(2,4)).toBe(0);
    expect(game.get_case_board(3,1)).toBe(2);
    expect(game.get_case_board(6,0)).toBe(0);

    expect(game.is_finished()).toBe(false);
  });

  test('set_stack_1', () => {
    let game = new OpenXum.Lot.Engine();

    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,2, 1);
    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,3, 6);
    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,0, 5);

    game._set_stack(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE, 2, 1));

    expect(game.get_case_board(2,1)).toBe(3);
    expect(game.get_case_board(3,6)).toBe(-1);
    expect(game.get_case_board(0,5)).toBe(-1);

    expect(game.is_finished()).toBe(false);
  });

  test('set_stack_2', () => {
    let game = new OpenXum.Lot.Engine();

    game.set_case_board(OpenXum.Lot.Color.WHITE,3, 3);
    game.set_case_board(OpenXum.Lot.Color.WHITE,4, 3);

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 5, 3));

    expect(game.get_case_board(3,3)).toBe(5);
    expect(game.get_case_board(4,3)).toBe(5);
    expect(game.get_case_board(5,3)).toBe(5);

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE_STACK, 4, 3));

    expect(game.get_case_board(3,3)).toBe(-1);
    expect(game.get_case_board(4,3)).toBe(3);
    expect(game.get_case_board(5,3)).toBe(-1);

    expect(game.is_finished()).toBe(false);
  });

  test('select_line_1', () => {
    let game = new OpenXum.Lot.Engine();

    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,6, 2);
    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,6, 3);
    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,6, 4);
    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,6, 5);
    game.set_case_board(OpenXum.Lot.Color.WHITE_TEMP,6, 6);

    game._select_line(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 3));
    game._select_line(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 5));
    game._select_line(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 4));

    expect(game.get_case_board(6,2)).toBe(2);
    expect(game.get_case_board(6,3)).toBe(5);
    expect(game.get_case_board(6,4)).toBe(5);
    expect(game.get_case_board(6,5)).toBe(5);
    expect(game.get_case_board(6,6)).toBe(2);

    expect(game.is_finished()).toBe(false);
  });

  test('select_line_2', () => {
    let game = new OpenXum.Lot.Engine();

    game.set_case_board(OpenXum.Lot.Color.WHITE,6, 2);
    game.set_case_board(OpenXum.Lot.Color.WHITE,6, 3);
    game.set_case_board(OpenXum.Lot.Color.WHITE,6, 5);
    game.set_case_board(OpenXum.Lot.Color.WHITE,6, 6);

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 6, 4));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 2));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 4));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 6));

    expect(game.get_case_board(6,2)).toBe(5);
    expect(game.get_case_board(6,3)).toBe(5);
    expect(game.get_case_board(6,4)).toBe(5);
    expect(game.get_case_board(6,5)).toBe(5);
    expect(game.get_case_board(6,6)).toBe(5);

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 5));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 6, 4));

    expect(game.get_case_board(6,2)).toBe(2);
    expect(game.get_case_board(6,3)).toBe(5);
    expect(game.get_case_board(6,4)).toBe(5);
    expect(game.get_case_board(6,5)).toBe(5);
    expect(game.get_case_board(6,6)).toBe(2);

    expect(game.is_finished()).toBe(false);
  });

  test('win', () => {
    let game= new OpenXum.Lot.Engine();

    game.set_case_board(OpenXum.Lot.Color.WHITE_STACK,1, 5);
    game.set_case_board(OpenXum.Lot.Color.WHITE_STACK,2, 4);
    game.set_case_board(OpenXum.Lot.Color.WHITE,2, 2);
    game.set_case_board(OpenXum.Lot.Color.WHITE,3, 3);

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 4, 4));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE, 3, 3));

    expect(game.is_finished()).toBe(true);
    expect(game.winner_is()).toBe(OpenXum.Lot.Color.WHITE);
  });

  test('draw', () => {
    let game= new OpenXum.Lot.Engine();

    game.set_number_piece(OpenXum.Lot.Color.WHITE, 3)
    game.set_number_piece(OpenXum.Lot.Color.BLACK, 3)

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 4, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 4, 5));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 6, 6));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 0, 0));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 6, 0));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 0, 6));


    expect(game.is_finished()).toBe(true);
    expect(game.winner_is()).toBe(OpenXum.Lot.Color.GREY);
  });

  test('game_test', () => {
    let game = new OpenXum.Lot.Engine();

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 0));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 0, 0));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 2));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 3, 0));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 1));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE_STACK, 1, 2));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 4, 2));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 2, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 6, 4));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 3, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 6, 2));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE_STACK, 1, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 4, 4));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 0, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 5, 3));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.BLACK_TEMP, 6, 2));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.BLACK_TEMP, 5, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.BLACK_TEMP, 4, 4));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.BLACK_STACK, 5, 3));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 3, 6));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 0, 5));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 2, 5));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.BLACK, 1, 6));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 4));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_TEMP, 0, 3));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_TEMP, 2, 5));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_TEMP, 3, 6));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE_STACK, 1, 4));

    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_TEMP, 3, 6));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_TEMP, 2, 5));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_TEMP, 1, 4));
    game.move(new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.WHITE_STACK, 1, 4));

    expect(game.is_finished()).toBe(true);
    expect(game.winner_is()).toBe(OpenXum.Lot.Color.WHITE);
  });
});

describe('move', () => {
  test('getters', () => {
    let move0 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.GREY, 1, 2);
    let move1 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.BLACK_STACK, 3, 4);
    let move2 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE_STACK, 5, 6);

    expect(move0.get_type()).toBe(0);
    expect(move1.get_type()).toBe(1);
    expect(move2.get_type()).toBe(2);

    expect(move0.get_color()).toBe(-2);
    expect(move1.get_color()).toBe(1);
    expect(move2.get_color()).toBe(3);

    expect(move0.get_abs()).toBe(1);
    expect(move1.get_abs()).toBe(3);
    expect(move2.get_abs()).toBe(5);

    expect(move0.get_ord()).toBe(2);
    expect(move1.get_ord()).toBe(4);
    expect(move2.get_ord()).toBe(6);
  });

  test('encode', () => {
    let move1 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 2);
    let move2 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.BLACK, 3, 4);
    let move3 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 5, 6);

    expect(move1.get_color()).toBe(2);
    expect(move2.get_color()).toBe(0);
    expect(move3.get_color()).toBe(2);

    expect(move1.encode()).toBe('PW12');
    expect(move2.encode()).toBe('SB34');
    expect(move3.encode()).toBe('LW');
  });

  test('to_string', () => {
    let move1 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.PUT_PIECE, OpenXum.Lot.Color.WHITE, 1, 2);
    let move2 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SET_STACK, OpenXum.Lot.Color.BLACK, 3, 4);
    let move3 = new OpenXum.Lot.Move(OpenXum.Lot.MoveType.SELECT_LINE, OpenXum.Lot.Color.WHITE, 5, 6);

    expect(move1.get_color()).toBe(2);
    expect(move2.get_color()).toBe(0);
    expect(move3.get_color()).toBe(2);

    expect(move1.to_string()).toBe('Put White piece at 1,2');
    expect(move2.to_string()).toBe('Set Black stack at 3,4');
    expect(move3.to_string()).toBe('Select White line of three');
  });
});