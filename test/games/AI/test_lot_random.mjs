require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;
let cp1 = 0, cp2 = 0, cp3 = 0;
for(let i = 0 ; i < 10000 ; i++) {
  let e = new OpenXum.Lot.Engine(OpenXum.Lot.GameType.STANDARD, OpenXum.Lot.Color.WHITE);
  let p1 = new AI.Generic.RandomPlayer(OpenXum.Lot.Color.WHITE, OpenXum.Lot.Color.BLACK, e);
  let p2 = new AI.Generic.RandomPlayer(OpenXum.Lot.Color.BLACK, OpenXum.Lot.Color.WHITE, e);
  let p = p1;
  let moves = [];

  while (!e.is_finished()) {
    const move = p.move();

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
  }

  e.winner_is() === OpenXum.Lot.Color.WHITE ? cp1++ : e.winner_is() === OpenXum.Lot.Color.BLACK ? cp2++ : cp3++;
  console.log("Winner is " + (e.winner_is() === OpenXum.Lot.Color.WHITE ? "White" : e.winner_is() === OpenXum.Lot.Color.BLACK ? "Black" : "Grey"));
}
console.log(cp1, cp2, cp3);
/*for (let index = 0; index < moves.length; ++index) {
  console.log(moves[index].to_string());
}*/