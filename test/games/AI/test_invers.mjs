require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let win = [0, 0];
for (let i = 0; i < 10; ++i) {
  let e = new OpenXum.Invers.Engine(OpenXum.Invers.GameType.STANDARD, OpenXum.Invers.Color.RED);
  let p1 = new AI.Specific.Invers.MCTSPlayer(OpenXum.Invers.Color.RED, OpenXum.Invers.Color.YELLOW, e);
  let p2 = new AI.Generic.RandomPlayer(OpenXum.Invers.Color.YELLOW, OpenXum.Invers.Color.RED, e);
  let p = p1;

  while (!e.is_finished()) {
    let move = p.move();

    e.move(move);
    p = p === p1 ? p2 : p1;
  }
  ++win[e.winner_is()];
}

console.log("Random: " + win[OpenXum.Invers.Color.YELLOW] + " wins");
console.log("MCTS: " + win[OpenXum.Invers.Color.RED] + " wins");